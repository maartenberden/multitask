package android.multitask;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.multitask.firebaseHelper.Database;
import android.multitask.firebaseHelper.OnGetDataListener;
import android.multitask.list.TasklistActivity;
import android.multitask.models.Task;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.GenericTypeIndicator;

import java.io.Serializable;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCheckInforInServer("tasks");
    }

    @Override
    protected void onStart() {
        super.onStart();
        mCheckInforInServer("tasks");
    }

    private void mCheckInforInServer(String child) {
        new Database().mReadDataOnce(child, new OnGetDataListener() {
            @Override
            public void onStart() {
                //DO SOME THING WHEN START GET DATA HERE
            }

            @Override
            public void onSuccess(DataSnapshot data) {
                GenericTypeIndicator<ArrayList<Task>> t = new GenericTypeIndicator<ArrayList<Task>>() {};
                ArrayList<Task> tasks = data.getValue(t);
                assert tasks != null;
                for (int i = 0; i < tasks.size(); i++) {
                    tasks.get(i).setIndex(i);
                }

                Intent intent = new Intent(MainActivity.this, TasklistActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("Tasks", tasks);
                intent.putExtra("ABUNDLE",args);
                startActivity(intent);
            }

            @Override
            public void onFailed(DatabaseError databaseError) {
                makeSnackBar("Getting tasks failed");
            }
        });

    }

    public void makeSnackBar(String message) {
        Snackbar.make(this.findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).setAction("", null).show();
    }

}