package android.multitask.service;

import android.util.Log;

import java.sql.Timestamp;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateMapper {

    public DateMapper(){}

    public String getDateWithMillis(long millis){
        Date date = new Date(millis * 1000);
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a, MMM d");
        format.setTimeZone(TimeZone.getTimeZone("CET"));
        return format.format(date);
    }
}
