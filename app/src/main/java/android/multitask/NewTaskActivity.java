package android.multitask;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.multitask.models.Task;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class NewTaskActivity extends AppCompatActivity {
    private String tag = "##########";
    private TextView deadlineDate;
    private TextView deadlineTime;
    private TextView deadlineLabel;
    private TextView newTaskInputTitle;
    private Calendar cal;
    private Button deadlineButton;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener;

    private ArrayList<Task> taskModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);

        Button save = findViewById(R.id.newTaskButtonSave);
        Button back = findViewById(R.id.backButton);
        this.deadlineButton = findViewById(R.id.newTaskButtonDeadline);
        this.deadlineLabel = findViewById(R.id.newTaskdeadlineLabel);
        this.newTaskInputTitle = findViewById(R.id.newTaskInputTitle);
        this.deadlineDate = findViewById(R.id.newTaskInputDate);
        this.deadlineTime = findViewById(R.id.newTaskInputTime);
        this.cal = Calendar.getInstance();

        deadlineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(deadlineLabel.getVisibility() == View.VISIBLE) {
                    clearDeadline();
                } else {
                    displayDatePicker();
                }
            }
        });

        deadlineDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayDatePicker();
            }
        });

        deadlineTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayTimePicker();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Task task = new Task();
                if(newTaskInputTitle.getText().toString().length() < 3) {
                    Toast toast = Toast.makeText(getApplicationContext(), "task must have at least 3 chars", Toast.LENGTH_LONG);
                    toast.show();
                } else {
                    task.setTitle(newTaskInputTitle.getText().toString());
                    task.setFinished(0);
                    if(deadlineLabel.getVisibility() == View.VISIBLE) {
                        task.setDeadline(cal.getTimeInMillis() / 1000);
                    }

                    Intent intent = getIntent();
                    Bundle args = intent.getBundleExtra("BUNDLE");
                    ArrayList<Task> tasksArrayList = (ArrayList<Task>) args.getSerializable("Tasks");

                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference myRef = database.getReference("tasks");
                    myRef.child(String.valueOf(tasksArrayList.toArray().length)).setValue(task);

                    openMainActivity();
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                if (deadlineDate.getVisibility() == View.GONE) {
                    deadlineDate.setVisibility(View.VISIBLE);
                    deadlineLabel.setVisibility(View.VISIBLE);
                }
                String myFormat = "dd/MMM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                cal.set(y,m,d);
                deadlineDate.setText(sdf.format(cal.getTime()));
                if(deadlineTime.getText().toString().matches("")) {
                    displayTimePicker();
                }
            }
        };


        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int h, int m) {
                if (deadlineTime.getVisibility() == View.GONE) {
                    deadlineTime.setVisibility(View.VISIBLE);
                }
                cal.set(Calendar.HOUR, h);
                cal.set(Calendar.MINUTE, m);
                String myFormat = "HH:mm"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.FRENCH);
                deadlineTime.setText(sdf.format(cal.getTime()));
                if(deadlineTime.getText().toString().isEmpty() == false) {
                    deadlineButton.setText("clear deadline");
                }
            }
        };

    }

    private void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void clearDeadline() {
        deadlineDate.setVisibility(View.GONE);
        deadlineTime.setVisibility(View.GONE);
        deadlineLabel.setVisibility(View.GONE);
        deadlineDate.setText(null);
        deadlineTime.setText(null);
        deadlineButton.setText("set deadline");
    }

    private void displayDatePicker() {
        int year = cal.get(cal.YEAR);
        int month = cal.get(cal.MONTH);
        int day = cal.get(cal.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this,
            android.R.style.Theme_Holo_Light_Dialog_MinWidth,
            mDateSetListener,
            year, month, day
        );
        dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE,null, (DialogInterface.OnClickListener) null);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void displayTimePicker() {
        int h = cal.get(cal.HOUR_OF_DAY);
        int m = cal.get(cal.MINUTE);

        TimePickerDialog dialog = new TimePickerDialog(this, mTimeSetListener, h, m, true);
        dialog.setButton(TimePickerDialog.BUTTON_NEGATIVE,null, (DialogInterface.OnClickListener) null);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
}