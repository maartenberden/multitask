package android.multitask.list;

import android.content.Intent;
import android.multitask.NewTaskActivity;
import android.multitask.R;
import android.multitask.models.Task;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class HistorylistActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historylist);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("CBUNDLE");
        ArrayList<Task> taskModels = (ArrayList<Task>) args.getSerializable("Tasks");
        ArrayList<Task> allTasks = (ArrayList<Task>) args.getSerializable("AllTasks");

        Button newTaskButton = findViewById(R.id.newTaskButton);
        Button tasklistButton = findViewById(R.id.tasklistButton);

        newTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                Bundle args = intent.getBundleExtra("BUNDLE");
                intent = new Intent(getApplicationContext(), NewTaskActivity.class);
                intent.putExtra("BUNDLE",args);
                startActivity(intent);
            }
        });

        tasklistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ListView mListView = findViewById(R.id.list_view);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast t = Toast.makeText(HistorylistActivity.this,
                        "Click" + position,Toast.LENGTH_LONG);
                t.show();
            }
        });
        HistoryListAdapter mAdapter = new HistoryListAdapter(HistorylistActivity.this, 0, taskModels);
        mAdapter.setAllTasks(allTasks);
        mListView.setAdapter(mAdapter);
    }
}