package android.multitask.list;

import android.content.Context;
import android.content.Intent;
import android.multitask.MainActivity;
import android.multitask.R;
import android.multitask.models.Task;
import android.multitask.service.DateMapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class HistoryListAdapter extends ArrayAdapter<Task> {

    DateMapper dateMapper =  new DateMapper();
    ArrayList<Task> objects;
    private Context context;
    ArrayList<Task> allTasks = new ArrayList<>();

    public HistoryListAdapter(Context context, int resource, ArrayList<Task> objects){
        super(context, resource, objects);
        this.objects = objects;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder vh;
        if (convertView == null ) {
            vh = new ViewHolder();
            LayoutInflater li = LayoutInflater.from(getContext());
            convertView = li.inflate(R.layout.history_row, parent, false);
            vh.title = convertView.findViewById(R.id.subject_title);
            vh.deadline = convertView.findViewById(R.id.subject_time);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final Task task = getItem(position);
        assert task != null;
        vh.title.setText(task.getTitle());
        vh.deadline.setText(dateMapper.getDateWithMillis(task.getDeadline()));

        Button deleteBtn = convertView.findViewById(R.id.delete);
        deleteBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                allTasks.remove(task.getIndex());

                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("tasks");
                myRef.setValue(allTasks);
                context.startActivity(new Intent(context, MainActivity.class));
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        TextView title;
        TextView deadline;
    }

    public ArrayList<Task> getTaskObjects() {
        return this.objects;
    }

    public void setAllTasks(ArrayList<Task> allTasks) {
        this.allTasks = allTasks;
    }
}