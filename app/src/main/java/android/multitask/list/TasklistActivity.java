package android.multitask.list;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.multitask.MainActivity;
import android.multitask.NewTaskActivity;
import android.multitask.R;
import android.multitask.SettingsActivity;
import android.multitask.database.DatabaseHelper;
import android.multitask.database.DatabaseInfo;
import android.multitask.models.Task;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.io.Serializable;
import java.util.ArrayList;

public class TasklistActivity extends AppCompatActivity {

    ArrayList<Task> activeTasks = new ArrayList<>();
    ArrayList<Task> completedTasks = new ArrayList<>();
    ArrayList<Task> allTasks = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasklist);

        TextView textView = findViewById(R.id.userName);
        DatabaseHelper dbHelper = DatabaseHelper.getHelper(this);
        Cursor cursor = dbHelper.query(DatabaseInfo.SettingsTable.SETTINGSTABLE, new String[]{"*"},
                null, null, null, null, null);
        try {
            if(cursor.moveToLast()){
                textView.setText(cursor.getString(cursor.getColumnIndex("name")));
            }
        } finally {
            try { cursor.close(); } catch (Exception ignore) {}
        }


        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("ABUNDLE");
        allTasks = (ArrayList<Task>) args.getSerializable("Tasks");

        for(int i = 0; i < allTasks.size(); i++) {
            if(allTasks.get(i).getFinished() == 0) {
                activeTasks.add(allTasks.get(i));
            } else {
                completedTasks.add(allTasks.get(i));
            }
        }

        Button newTaskButton = findViewById(R.id.newTaskButton);
        Button historyButton = findViewById(R.id.historyButton);
        ImageButton settingsButton = findViewById(R.id.settingsButton);

        newTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                Bundle args = intent.getBundleExtra("ABUNDLE");
                intent = new Intent(getApplicationContext(), NewTaskActivity.class);
                intent.putExtra("BUNDLE",args);
                startActivity(intent);
            }
        });

        historyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HistorylistActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("Tasks", completedTasks);
                args.putSerializable("AllTasks", allTasks);
                intent.putExtra("CBUNDLE",args);
                startActivity(intent);
            }
        });

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TasklistActivity.this, SettingsActivity.class));

            }
        });

        ListView mListView = findViewById(R.id.list_view);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast t = Toast.makeText(TasklistActivity.this,
                        "Click" + position,Toast.LENGTH_LONG);
                t.show();
            }
        });

        TaskListAdapter mAdapter = new TaskListAdapter(TasklistActivity.this, 0, activeTasks);
        mAdapter.setAllTasks(allTasks);
        mListView.setAdapter(mAdapter);
    }
}