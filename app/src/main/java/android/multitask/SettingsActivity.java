package android.multitask;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.multitask.database.DatabaseHelper;
import android.multitask.database.DatabaseInfo;
import android.multitask.models.Setting;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SettingsActivity extends AppCompatActivity {

    EditText textField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Button backButton = findViewById(R.id.backButton3);
        Button saveButton = findViewById(R.id.saveSettings);
        textField = findViewById(R.id.nameInputTextField);

        DatabaseHelper dbHelper = DatabaseHelper.getHelper(this);
        Cursor cursor = dbHelper.query(DatabaseInfo.SettingsTable.SETTINGSTABLE, new String[]{"*"},
                null, null, null, null, null);

        Setting setting = new Setting();

        try {
            if(cursor.moveToLast()){
                setting.setName(cursor.getString(cursor.getColumnIndex("name")));
            }
        } finally {
            try { cursor.close(); } catch (Exception ignore) {}
        }

        if(setting.getName() != null){
            textField.setText(setting.getName());
        } else {
            textField.setText(R.string.user);
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveToDatabase();
            }
        });
    }

    public void saveToDatabase() {
        DatabaseHelper dbHelper = DatabaseHelper.getHelper(this);
        String name = textField.getText().toString();

        ContentValues values = new ContentValues();
        values.put(DatabaseInfo.SettingsColumn.NAME, name);

        dbHelper.insert(DatabaseInfo.SettingsTable.SETTINGSTABLE, null, values);

        startActivity(new Intent(SettingsActivity.this, MainActivity.class));
    }
}