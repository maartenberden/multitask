package android.multitask.models;

import java.io.Serializable;

public class Task implements Serializable {
    String title;
    int finished;
    long deadline;
    int index;

    public Task() {}

    public Task(String title, int finished, long deadline) {
        this.title = title;
        this.finished = finished;
        this.deadline = deadline;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getFinished() {
        return finished;
    }

    public void setFinished(int finished) {
        this.finished = finished;
    }

    public long getDeadline() {
        return deadline;
    }

    public void setDeadline(long deadline) {
        this.deadline = deadline;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
