package android.multitask.database;

public class DatabaseInfo {
    public class SettingsTable {
        public static final String SETTINGSTABLE = "settings";
    }

    public class SettingsColumn {
        public static final String NAME = "name";
    }
}
